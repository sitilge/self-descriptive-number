package self_descriptive_number

import (
	"strconv"
	"strings"
	"sync"
)

//Base10 finds all 10-digit self descriptive numbers.
func Base10() ([]uint64, error) {
	var numbers []uint64

	var wg sync.WaitGroup
	wg.Add(9)

	//Create workers.
	for i := 1; i < 10; i++ {
		go func(mult int) {
			for j := uint64(1_000_000_000 * mult); j < uint64(1_000_000_000*(mult+1)); j++ {
				exists, number := Calculate(j)

				if exists {
					numbers = append(numbers, number)
				}
			}

			wg.Done()
		}(i)
	}

	//Wait for workers to finish.
	wg.Wait()

	return numbers, nil
}

//Calculate takes in an integer and checks
//if it is a valid self descriptive number.
func Calculate(num uint64) (bool, uint64) {
	number := strconv.FormatUint(num, 10)
	exploded := strings.Split(number, "")

	for i := 0; i < 10; i++ {
		digit, _ := strconv.Atoi(exploded[i])
		count := strings.Count(number, strconv.Itoa(i))

		if count != digit {
			return false, 0
		}
	}

	return true, num
}

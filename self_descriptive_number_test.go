package self_descriptive_number

import "testing"

func TestBase10(t *testing.T) {
	numbers, err := Base10()
	if err != nil {
		t.Fatal(err)
	}
	
	if len(numbers) != 1 {
		t.Log("number is not 1")
		t.Fail()
	}

	expected := uint64(6210001000)
	
	if numbers[0] != expected {
		t.Logf("actual: %v, expected: %v", numbers[0], expected)
		t.Fail()
	}
}